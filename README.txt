cacheclear is a simple module to force the "page cache" to be refreshed at a given time interval.  

Installation
------------

Simple copy the cachclear.module file into your modules directory

Credits
-------

Module developed by M Dixon, of http://www.computerminds.co.uk initially for the site http://www.booktribes.com
Then modyfied by Piotr Brzozowski (ZolV) for Drupal 5

